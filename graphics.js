
var Graphics = {
	makeFullscreen: function(canvas){
		$("body").css({
			overflow: "hidden",
			margin: "0px",
			padding: "0px"
		});
		
		canvas.width = $("body").innerWidth();
		canvas.height = $(window).innerHeight();
		canvas.style.top = "0px";
		canvas.style.left = "0px";
		
		return canvas;
	},
	
	init: function(args){
		this.canvas = args.canvas === undefined ? $("canvas")[0]: args.canvas;
		this.ctx = this.canvas.getContext("2d");
		
		var rootStyle = args === undefined || args.rootStyle === undefined ? undefined: args.rootStyle;
		var rootClickListener = args === undefined || args.rootClickListener === undefined ? undefined: args.rootClickListener;
		this.root = new GuiElement({
			name: "root",
			style: rootStyle,
			position:{
				x: 0,
				y: 0
			},
			containerType: ContainerType.RELATIVE,
			size: {
				width: this.canvas.width,
				height: this.canvas.height
			},
			clickListener: rootClickListener
		});
		
		this.canvas.addEventListener("mouseup", this.searchTarget); //TODO: support other events (touch, keys)
		this.canvas.addEventListener("mousedown", this.searchTarget);
		// document.addEventListener("click", function(event){
			// console.log(event.type);
		// });
		
		window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
		this.draw();
	},
	
	add: function(child){
		this.root.addChild(child);
	},
	
	draw: function(){
		this.root.drawAll(this.ctx, {
			x: 0,
			y: 0
		});
	
		window.requestAnimationFrame(function(){
			Graphics.draw();
		});
	},
	
	searchTarget: function(event){
	console.log(event.type);
		var x = event.pageX;
		var y = event.pageY;
		var element = Graphics.root;
		var deeper = true;
		while(deeper){
			//if(element.inside(x,y)){
				var children = element.children;
				deeper = false;
				for(var i = 0; i < children.length; i++){
					if(children[i].inside(x,y)){
						element.click(event, true);
						
						element = children[i];
						deeper = true;
					}
				}
			//}
		}
		// console.log(event.type);
		this.focused = element;
		element.click(event, false);
	},
	
	width: function(){
		return this.root.size.width;
	},
	
	height: function(){
		return this.root.size.height;
	}
};