var images = {};

function fetchImage(path, callback) {
	if(images[path] === undefined) { //The image we want to use was not yet retrieved
		var obj = {
			img : new Image(),
			loading : true,
			callbacks : []
		};
		obj.img.onload = function() {
			for(var i = 0; i < obj.callbacks.length; i++) {
				obj.callbacks[i](obj.img);
			}
			obj.loading = false;
			obj.callbacks = undefined;
		};
		obj.callbacks.push(callback);
		images[path] = obj;
		obj.img.src = path;
	}
	else {
		var obj = images[path];
		if(obj.loading) { //The image exists but is still loading
			obj.callbacks.push(callback);
		}
		else { //The image exists and was already loaded
			callback(obj.img);
		}
	}
}

/*
 * img = image to drawImage
 * offset = number of the picture from top (starting with 0)
 * ctx = context to draw onLine
 * positionAnchor = coordinates of the upper left hand corner (has attributes x and y)
 * middleSize = size of the repeated portion in the middle (has attributes width and height)
 */
function drawImage(img, offset, ctx, positionAnchor, middleSize){
	if(middleSize === undefined){
		middleSize = {
			width: img.width/3,
			height: img.width/3
		};
	}
	if(middleSize.width === undefined){
		middleSize.width = img.width/3;
	}
	if(middleSize.height === undefined){
		middleSize.height = img.width/3;
	}
	
	var imageOffset = img.height * offset/3;
	var width_3 = img.width / 3;
	
	ctx.drawImage(img, 0, imageOffset, //Draw upper left corner			
		width_3, width_3, 
		positionAnchor.x, positionAnchor.y, 
		width_3, width_3);
	ctx.drawImage(img, width_3 * 2, imageOffset, //Draw upper right corner		
		width_3, width_3, 
		positionAnchor.x + middleSize.width + width_3, positionAnchor.y, 
		width_3, width_3);
	ctx.drawImage(img, 0, width_3 * 2 + imageOffset,  //Draw lower left corner		
		width_3, width_3, 
		positionAnchor.x, positionAnchor.y + middleSize.height + width_3, 
		width_3, width_3);
	ctx.drawImage(img, width_3 * 2, width_3 * 2 + imageOffset,  //Draw lower right corner		
		width_3, width_3, 
		positionAnchor.x + middleSize.width + width_3, positionAnchor.y + middleSize.height + width_3, 
		width_3, width_3);
	ctx.drawImage(img, 0, width_3 + imageOffset,  //Draw left		
		width_3, width_3, 
		positionAnchor.x, positionAnchor.y + width_3,
		width_3, middleSize.height);
	ctx.drawImage(img, width_3 * 2, width_3 + imageOffset,  //Draw right	
		width_3, width_3, 
		positionAnchor.x + middleSize.width + width_3, positionAnchor.y + width_3, 
		width_3, middleSize.height);
	ctx.drawImage(img, width_3, imageOffset,  //Draw top
		width_3, width_3, 
		positionAnchor.x + width_3, positionAnchor.y, 
		middleSize.width, width_3);
	ctx.drawImage(img, width_3, width_3*2 + imageOffset,  //Draw bottom
		width_3, width_3, 
		positionAnchor.x + width_3, positionAnchor.y + middleSize.height + width_3, 
		middleSize.width, width_3);
	ctx.drawImage(img, width_3, width_3 + imageOffset,  //Draw center
		width_3, width_3, 
		positionAnchor.x + width_3, positionAnchor.y + width_3, 
		middleSize.width, middleSize.height);
}

var Coloring = Object.freeze({
	AVERAGE: "average",
	LIGHTEST: "lightest",
	DARKEST: "darkest"
});

/*
 * img = image to recolor
 * colors = colors 	{
 * 						border: color for the outer 1/3,
 * 						inner: color for the middle 1/3,
 * 						mode: coloring scheme. Any of Coloring (Coloring.AVERAGE is default)
 * 					}
 */
function colorImage(img, colors){
	if(colors === undefined || (colors.border === undefined && colors.inner === undefined)){
		//no recoloring should take place...
		return img;
	}
	
	if(colors.mode === undefined){
		colors.mode = Coloring.AVERAGE;
	}
	
	var canvas = document.createElement('canvas');
	document.body.appendChild(canvas);
	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);
	var imageData = ctx.getImageData(0,0, img.width, img.height);
	
	console.log(imageData);
	console.log(img.src + " vorher: " + imageData.data[20] + " " + imageData.data[21] + " " + imageData.data[22]);
	
	if(colors.border !== undefined){
		colors.border = colorToRGBArray(colors.border);
	}
	if(colors.inner !== undefined){
		colors.inner = colorToRGBArray(colors.inner);
	}
	
	for(var offset = 0; offset < img.height; offset += img.width){
		if(colors.border !== undefined){
			var anchor = getBorderColorAnchor(imageData.data, img.width, offset, colors.mode);
			var factors = [];
			
			for(var color = 0; color < anchor.length; color++){
				factors[color] = colors.border[color]/anchor[color];
			}
			console.log(factors);
			//top border
			for(var row = offset; row < offset + img.width/3; row++){
				for(var column = 0; column < img.width; column++){
					for(var color = 0; color < factors.length; color++){
						imageData.data[4 * (row * img.width + column) + color] *= factors[color];
					}
				}
			}
			//bottom border
			for(var row = offset + 2/3 * img.width; row < offset + img.width; row++){
				for(var column = 0; column < img.width; column++){
					for(var color = 0; color < factors.length; color++){
						imageData.data[4 * (row * img.width + column) + color] *= factors[color];
					}
				}
			}
			//left border
			for(var row = offset + 1/3 * img.width; row < offset + 2/3 * img.width; row++){
				for(var column = 0; column < img.width/3; column++){
					for(var color = 0; color < factors.length; color++){
						imageData.data[4 * (row * img.width + column) + color] *= factors[color];
					}
				}
			}
			//right border
			for(var row = offset + 1/3 * img.width; row < offset + 2/3 * img.width; row++){
				for(var column = 2/3 * img.width; column < img.width; column++){
					for(var color = 0; color < factors.length; color++){
						imageData.data[4 * (row * img.width + column) + color] *= factors[color];
					}
				}
			}
		}
		
		if(colors.inner !== undefined){
			var anchor = getInnerColorAnchor(imageData.data, img.width, offset, colors.mode);
			var factors = [];
			
			for(var color = 0; color < anchor.length; color++){
				factors[color] = anchor[color]/colors.inner[color];
			}
			
			for(var row = offset + img.width/3; row < offset + 2/3 * img.width; row ++){
				for(var column = img.width/3; column < 2/3 * img.width; column++){
					for(var color = 0; color < factors.length; color++){
						imageData.data[4 * (row * img.width + column) + color] *= factors[color];
					}
				}
			}
		}
	}
	
	console.log(img.src + " nachhar: " + imageData.data[20] + " " + imageData.data[21] + " " + imageData.data[22]);
	
	ctx.putImageData(imageData, 0, 0);
	var dataURL = canvas.toDataURL();
	var image = new Image();
	image.src = dataURL;
	image.width = img.width;
	image.height = img.height;
	return image;
}

function getBorderColorAnchor(data, size, offset, mode){
	var colors = [0,0,0];
	
	//top border
	for(var row = offset; row < offset + size/3; row++){
		for(var column = 0; column < size; column++){
			for(var color = 0; color < colors.length; color++){
				var current = data[4 * (row * size + column) + color];
				switch(mode){
					case Coloring.AVERAGE:
						colors[color] += current;
						break;
					case Coloring.LIGHTEST:
						if(colors[color] > current){
							colors[color] = current;
						}
						break;
					case Coloring.DARKEST:
						if(colors[color] < current){
							colors[color] = current;
						}
						break;
				}
			}
		}
	}
	//bottom border
	for(var row = offset + 2/3 * size; row < offset + size; row++){
		for(var column = 0; column < size; column++){
			for(var color = 0; color < colors.length; color++){
				var current = data[4 * (row * size + column) + color];
				switch(mode){
					case Coloring.AVERAGE:
						colors[color] += current;
						break;
					case Coloring.LIGHTEST:
						if(colors[color] > current){
							colors[color] = current;
						}
						break;
					case Coloring.DARKEST:
						if(colors[color] < current){
							colors[color] = current;
						}
						break;
				}
			}
		}
	}
	//left border
	for(var row = offset + 1/3 * size; row < offset + 2/3 * size; row++){
		for(var column = 0; column < size/3; column++){
			for(var color = 0; color < colors.length; color++){
				var current = data[4 * (row * size + column) + color];
				switch(mode){
					case Coloring.AVERAGE:
						colors[color] += current;
						break;
					case Coloring.LIGHTEST:
						if(colors[color] > current){
							colors[color] = current;
						}
						break;
					case Coloring.DARKEST:
						if(colors[color] < current){
							colors[color] = current;
						}
						break;
				}
			}
		}
	}
	//right border
	for(var row = offset + 1/3 * size; row < offset + 2/3 * size; row++){
		for(var column = 2/3 * size; column < size; column++){
			for(var color = 0; color < colors.length; color++){
				var current = data[4 * (row * size + column) + color]
				switch(mode){
					case Coloring.AVERAGE:
						colors[color] += current;
						break;
					case Coloring.LIGHTEST:
						if(colors[color] > current){
							colors[color] = current;
						}
						break;
					case Coloring.DARKEST:
						if(colors[color] < current){
							colors[color] = current;
						}
						break;
				}
			}
		}
	}
	
	if(mode == Coloring.AVERAGE){
		for(var color = 0; color < colors.length; color++){
			colors[color] /= 2 * (size* (size/3)) + 2 * (size/3 * size/3);
		}
	}
	
	return colors;
}

function getInnerColorAnchor(data, size, offset, mode){
	var colors = [0,0,0];
	
	for(var row = offset + size/3; row < offset + 2/3 * size; row ++){
		for(var column = size/3; column < 2/3 * size; column++){
			for(var color = 0; color < colors.length; color++){
				var current = data[4 * (row * size + column) + color]
				switch(mode){
					case Coloring.AVERAGE:
						colors[color] += current;
						break;
					case Coloring.LIGHTEST:
						if(colors[color] > current){
							colors[color] = current;
						}
						break;
					case Coloring.DARKEST:
						if(colors[color] < current){
							colors[color] = current;
						}
						break;
				}
			}
		}
	}
	
	if(mode == Coloring.AVERAGE){
		for(var color = 0; color < colors.length; color++){
			colors[color] /= size/3 * size/3;
		}
	}
	
	return colors;
}