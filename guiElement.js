var ContainerType = Object.freeze({
	RELATIVE: "relative",
	DYNAMIC: "dynamic",
	LEAF: "leaf"
});

var Size = Object.freeze({
	STATIC: "static",
	DYNAMIC: "dynamic"
});

function inherit(object, from, args, overwrite){
	var parent = new from(args);
	
	for(attr in parent){
		object[attr] = parent[attr];
	}
	
	for(attr in overwrite){
		object[attr] = overwrite[attr];
	}
}

function GuiElement(args){
	this.parseArguments(args);
};

GuiElement.prototype = {
	constructor: GuiElement,
	defaultValues: {
		style: {
			foregroundcolor: "black",
			backgroundcolor: "white"
		},
		size: {
			width: 100,
			height: 100,
			padding: 5,
			mode: Size.STATIC
		},
		position: {
			x: 0,
			y: 0
		},
		containerType: ContainerType.RELATIVE,
		layout: function(children, index){ //vertical Linear Layout (put all items exactly beneath each other in adding order)
			var offset = 0;
			for(var i = 0; i < index; i++){
				offset += children[i].size.current.height;
			}
			return {
				x: 0,
				y: offset
			};
		}
	},
	
	parser: {
		guielement: function(object, args){
			object.children = [];
	
			object.name = args === undefined || args.name === undefined ? "element": args.name;
		
			object.fillInSetting("size", args);
			if(object.size.height <= 0 || object.size.width <= 0){
				throw "Size of " + object.name + " must be positive. It currently is {width: " + object.size.width + ", height: " + object.size.height + "}";
			}
			
			object.containerType = args === undefined || args.containerType === undefined ? (object.containerType === undefined ? object.defaultValues.containerType: object.containerType): args.containerType; 
			if(object.containerType == ContainerType.DYNAMIC){
				object.layout = args === undefined || args.layout === undefined ? object.defaultValues.layout: args.layout;
			}
			else if(object.containerType === ContainerType.LEAF){
				object.addChild = function(child){
					throw "Cannot add child to leaf element";
				}; //object is a leaf element, so redefine addChild-method, so that nobody can add a child and is informed about it
			}
			
			object.fillInSetting("position", args);
			if(object.position.x < 0 || object.position.y < 0){
				throw "Position of " + object.name + " x and y must be greater or equal to 0. It currently is: {x: " + object.position.x + ", y: " + object.position.y + "}";
			}
			
			object.fillInSetting("style", args);
			
			object.fillInArraySetting("clickListener", args);
			
			//load image if needed
			if(object.style.imagename !== undefined){
				object.style.image = undefined;
				fetchImage(object.style.imagename, function(img) {
					// object.style.image = colorImage(img, {
						// border: object.style.foregroundcolor,
						// inner: object.style.backgroundcolor,
						// mode: Coloring.DARKEST
					// });
					fetchImage(object.style.imagename, function(img){
						object.style.image = img;
					});
				});
			}
		}
	},
	
	parseArguments: function(args){
		var name = this.constructor.toString().split(" ")[1].split("(")[0].toLowerCase();
		this.parser[name](this, args);
	},
	
	setParser: function(parserFunction){
		var name = this.constructor.toString().split(" ")[1].split("(")[0].toLowerCase();
		this.parser[name] = parserFunction;
	},
	
	addDefaultSettings: function(setting){
		for(name in this.defaultValues[setting]){
			if(this[setting][name] === undefined){
				this[setting][name] = this.defaultValues[setting][name];
			}
		}
	},
	
	fillInSetting: function(setting, args){
		if(this[setting] === undefined){
				this[setting] = {};
		}
		if(args !== undefined && args[setting] !== undefined){
			for(name in args[setting]){
				this[setting][name] = args[setting][name];
			}
		}
		this.addDefaultSettings(setting);
	},
	
	fillInArraySetting: function(setting, args){
		if(this[setting] === undefined){
				this[setting] = [];
		}
		if(args !== undefined && args[setting] !== undefined){
			// for(element in args[setting]){
				// this[setting].push(args[setting][element]);
			// }
			this[setting] = this[setting].concat(args[setting]);
		}
		//this.addDefaultSettings(setting);
	},
	
	drawAll: function(ctx, positionAnchor){
		this.position.lastAnchor = positionAnchor;
		this.draw(ctx, positionAnchor);
		
		for(var i = 0; i < this.children.length; i++){
			var child = this.children[i];
			
			var position = {x: 0, y: 0};
			switch(this.containerType){
				case ContainerType.RELATIVE:
					position.x = child.position.x;
					position.y = child.position.y;
					break;
				case ContainerType.DYNAMIC:
					if(this.layout === undefined){
						throw "Layout-handler is missing in dynamic container";
					}
					else{
						position = this.layout(this.children, i);
					}
					break;
				default:
					throw "illegal positioning style: " + this.position.style;
			}
			
			position.x += positionAnchor.x;
			position.y += positionAnchor.y;
			child.drawAll(ctx, position);
		}
		
		if(this.containerType !== ContainerType.LEAF){
			var overflow = false;
			var maxSize = this.size.current === undefined ? this.calculateSize(): this.size.current;
			maxSize.width = maxSize.width + this.position.lastAnchor.x;
			maxSize.height = maxSize.height + this.position.lastAnchor.y;
			for(var i = 0; i < this.children.length; i++){
				var child = this.children[i];
				var childX = child.position.lastAnchor.x;
				var childY = child.position.lastAnchor.y;
				var childWidth = child.size.current.width;
				var childHeight = child.size.current.height;
				if(childX + childWidth > maxSize.width){
					maxSize.width = childX + childWidth + this.size.padding;
					overflow = true;
				}
				if(childY + childHeight > maxSize.height){
					maxSize.height = childY + childHeight + this.size.padding;
					overflow = true;
				}
				
				if(overflow && this.size.mode == Size.STATIC){
					throw "Elements must not be larger than their static container!. " + child.name + " is larger than its parent."
				}
			}
			maxSize.width = maxSize.width - this.position.lastAnchor.x;
			maxSize.height = maxSize.height - this.position.lastAnchor.y;
			this.size.current = maxSize;
			if(overflow && this.size.mode == Size.DYNAMIC){
				this.drawAll(ctx, positionAnchor);
			}
		}
		else{
			this.size.current = this.calculateSize();
		}
	},
	
	draw: function(ctx, positionAnchor){
		ctx.strokeStyle = this.style.foregroundcolor;
		ctx.fillStyle = this.style.backgroundcolor;
		var size = this.size.current === undefined ? this.size: this.size.mode == Size.DYNAMIC ? this.size.current: this.size;
		ctx.fillRect(positionAnchor.x, positionAnchor.y, size.width, size.height);
		ctx.strokeRect(positionAnchor.x, positionAnchor.y, size.width, size.height);
	},
	
	addChild: function(child){
		this.children.push(child);
	},
	
	calculateSize: function(){
		return {
			width: this.size.width, 
			height: this.size.height
		};
	},
	
	inside: function(x, y){
		var thisX = this.position.lastAnchor.x;
		var thisY = this.position.lastAnchor.y;
		var thisWidth = this.size.current.width;
		var thisHeight = this.size.current.height;
		return x >= thisX && x <= thisX + thisWidth && y >= thisY && y <= thisY + thisHeight;
	},
	
	addClickListener: function(listener){
		this.clickListener.push(listener);
	},
	
	click: function(event, intercept){
		for(l in this.clickListener){
			var listener = this.clickListener[l];
			//if no type is defined, we assume "mouseup", because things should be activated at mouseup by default. Also if intercept is not defined, it is set to false
			if(event.type == (listener.type === undefined ? "mouseup": listener.type) && intercept == (listener.intercept === undefined ? false: listener.intercept)){
				listener.click(this, event);
			}
		}
	}
};