function Label(args){
	inherit(this, GuiElement,{
		name: "label",
		text: "Label",
		containerType: ContainerType.LEAF
	}, {
		constructor: Label,
		draw: function(ctx, positionAnchor){
			this.drawText(ctx, {
				x: positionAnchor.x,
				y: positionAnchor.x + this.style.fontsize
			});
		},
		calculateSize: function(){
			return {
				width: this.size.textwidth,
				height: this.style.fontsize
			};
		}
	});
	this.setParser(function(object, args){
		object.text = args === undefined || args.text === undefined ? this.text: args.text;
		object.parser.guielement(object, args);
	});
	
	this.parseArguments(args);
}

//Label.prototype = new GuiElement();

// Label.prototype.;

//Label.prototype.parser.label = ;

// Label.prototype.

// Label.prototype.;

Label.prototype.drawText = function(ctx, position, baseline){
	ctx.font = this.style.fontstyle + " " + this.style.fontsize + "px " + this.style.font;
	var textSize = ctx.measureText(this.text);
	textSize.height = this.style.fontsize;
	
	//this is for the calculateSize method, so we don't need to measure the text again each time we want to determine the size of the button
	this.size.textwidth = textSize.width;
	
	if(baseline !== undefined){
		ctx.textBaseline = baseline;
	}
	ctx.fillStyle = this.style.foregroundcolor;
	ctx.fillText(this.text, position.x, position.y);
	ctx.fillStyle = this.style.backgroundcolor;
}