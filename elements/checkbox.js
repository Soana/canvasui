
function Checkbox(args){
	inherit(this, Button, {
		name: "checkbox",
		text: "Checkbox",
		containerType: ContainerType.LEAF,
		style: {
			imagename: "images/checkbox.png",
			foregroundcolor: "black"
		},
		size:{
			boxdistance: 5
		},
		clickListener: [
			{
				type: "mouseup",
				click: function(object, event){
					object.selected = ! object.selected;
					for(index in object.clickListener){
						if(object.clickListener[index].type == "select"){
							object.clickListener[index].click(object);
						}
					}
				}
			}
		]
	}, {
		constructor: Checkbox,
		draw: function(ctx, positionAnchor){
			if(this.style.image !== undefined){
				drawImage(this.style.image, this.selected ? 2 : 0, ctx, positionAnchor);
				
				this.drawText(ctx, {
					x: positionAnchor.x + this.style.image.width + this.size.boxdistance,
					y: positionAnchor.y + this.style.image.width/2
				},"middle");
			}
		},
		calculateSize: function(){
			if(this.style.image === undefined){
				return {
					width: 0,
					height: 0
				};
			}
			else{
				return {
					width: this.style.image.width + this.size.boxdistance + this.size.textwidth,
					height: this.style.image.width > this.style.fontsize ? this.style.image.width: this.style.fontsize
				};
			}
		}
	});
	this.setParser(function(object, args){
		object.selected = args === undefined || args.selected === undefined ? false: args.selected;
		object.parser.button(object, args);
	});
	this.parseArguments(args);
};

// Checkbox.prototype = new Button();

// Checkbox.prototype.;
// Checkbox.prototype.parser.checkbox = ;

// Checkbox.prototype.;
// Checkbox.prototype.;