
function RadioGroup(args){
	inherit(this, GuiElement, {
		containerType: ContainerType.DYNAMIC,
		size: {
			mode: Size.DYNAMIC
		}
	},{
		constructor: RadioGroup,
		draw: function(ctx, positionAnchor){
			//do not draw this container. It is only for logical purposes.
		},
		calculateSize: function(){
			var width = 0, height = 0;
			if(this.children.length != 0){
				width = this.children[0].size.current.width;
				height = this.children[0].size.current.height;
				
				for(var i = 0; i < this.children.length; i++){
					if(this.children[i].size.current.width > width){
						width = this.children[i].size.current.width;
					}
					height += this.children[i].size.current.height;
				}
			}
			
			return {
				width: width,
				height: height
			};
		},
		addChild: function(child){
			if(child instanceof RadioButton){
				child.parent = this;
				this.children.push(child);
			}
			else{
				throw "You can only add RadioButton types to the radiogroup";
			}
		}
	});
	this.setParser(function(object, args){
		object.selected = 0;
		object.parser.guielement(object, args);
	});
	
	this.parseArguments(args);
	
	if(args === undefined || args.children === undefined){
		throw "A RadioGroup must be initialized with at least 1 child. (Do not forget to add the children parameter)";
	}
	else{
		for(var i = 0; i < args.children.length; i++){
			this.addChild(args.children[i]);
		}
		if(args === undefined || args.selected === undefined){
			
			this.select(0);
		}
		else{
			this.select(args.selected);
		}
	}
}

// RadioGroup.prototype.;

RadioGroup.prototype.select = function(object){
	if(object instanceof RadioButton){
		for(var i = 0; i < this.children.length; i++){
			if(this.children[i] == object){
				this.children[this.selected].selected = false;
				this.selected = i;
				object.selected = true;
			}
		}
	}
	else{ //object should be a number. Probably programmer selected an element programmatically
		this.children[this.selected].selected = false;
		this.selected = object;
		this.children[object].selected = true;
	}
}