
function RadioButton(args){
	inherit(this, Checkbox, {
		name: "radiobutton",
		text: "Radiobutton",
		containerType: ContainerType.LEAF,
		style: {
			imagename: "images/radiobutton.png",
			foregroundcolor: "black"
		},
		clickListener: [
			{
				type: "select",
				click: function(object){
					if(object.parent === undefined){
						throw "Radiobutton (" + object.name + ") with text \"" + object.text + "\" was selected, but has no parent";
					}
					else{
						object.parent.select(object);
					}
				}
			}
		]
	}, {
		constructor: RadioButton
	});
	this.setParser(function(object, args){
		object.parser.checkbox(object, args);
	});
	
	this.parseArguments(args);
}

// RadioButton.prototype = new Checkbox();

// RadioButton.prototype.;

// RadioButton.prototype.parser.radiobutton = ;

