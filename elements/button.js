
function Button(args){
	inherit(this, Label, {
		name: "button",
		text: "Button",
		containerType: ContainerType.LEAF,
		size: {
			mode: Size.DYNAMIC,
			cornerradius: 5
		},
		style: {
			foregroundcolor: "white",
			fontsize: 12,
			font: "Arial",
			imagename: "images/button.png",
			fontstyle: ""
		},
		clickListener : [
			{
				type : "mousedown",
				click : function (target, evt) {
					target.clicked = true;
				}
			}, {
				type : "mouseup",
				click : function (target, evt) {
					target.clicked = false;
				}
			}
		]
	}, {
		constructor: Button,
		draw: function(ctx, positionAnchor){
			if(this.style.image !== undefined){
				drawImage(this.style.image, this.clicked ? 0 : 2, ctx, positionAnchor, textSize);
				this.drawText(ctx, {
					x: positionAnchor.x + this.size.cornerradius,
					y: positionAnchor.y + this.size.cornerradius + textSize.height
				});
			}	
		},
		calculateSize: function(){
			if(this.style.image === undefined){
				return {
					width: 0,
					height: 0
				};
			}
			else{
				return {
					width: this.size.textwidth + 2* this.style.image.width/3,
					height: this.style.fontsize + 2* this.style.image.width/3
				};
			}
		}
	});
	this.setParser(function(object, args){
		object.parser.label(object, args);
	});
	this.parseArguments(args);
};

//Button.prototype = new Label();

// Button.prototype.; 
//Button.prototype.parser.button = ;

// Button.prototype.;

// Button.prototype.;

